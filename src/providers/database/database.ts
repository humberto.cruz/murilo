import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';
import pf from 'pouchdb-find';
PouchDB.plugin(pf);

//import DbSchema from './schema';

@Injectable()
export class DatabaseProvider {
  public dbLocal: any;
  public dbRemoto: any;
  public dataEstado: any;
  public dataCidade: any;
  public dataRamo: any;
  public dataCliente: any;

  constructor() {
    this.dbLocal = new PouchDB('murilodb');
    this.dbRemoto = 'http://admin:amme123123@159.65.252.20:5984/murilodb';
    let options ={
      live: true,
      retry: true,
      continuous: true
    };
    this.dbLocal.sync(this.dbRemoto, options);
    this.dbLocal.createIndex({
      index: {
        fields: ['_id','siglaEstado','nomeEstado']
      }
    });
  }

  public getEstados(ids=null) {
    return new Promise(resolve => {
      this.dbLocal.find({
        selector:{
          'table':'estado'
        }
      }).then((result) => {
        this.dataEstado = result;
        resolve(this.dataEstado);
      });
    });
  }

  public getCidades(selector={}) {
    selector['table'] = 'cidade';
    return new Promise(resolve => {
      this.dbLocal.find({
        selector:selector
      }).then((result) => {
        this.dataCidade = result;
        resolve(this.dataCidade);
      });
    });
  }

  public getRamos(selector={}) {
    selector['table'] = 'ramo';
    return new Promise(resolve => {
      this.dbLocal.find({
        selector:selector
      }).then((result) => {
        this.dataRamo = result;
        resolve(this.dataRamo);
      });
    });
  }

  public getClientes(selector={}) {
    selector['table'] = 'cliente';
    return new Promise(resolve => {
      this.dbLocal.find({
        selector:selector
      }).then((result) => {
        this.dataCliente = result;
        resolve(this.dataCliente);
      });
    });
  }

  public salvarEstado(estado: any){
    if (!estado['table']) estado['table'] = 'estado';
    this.dbLocal.post(estado).then((res)=>{
      estado._rev = res.rev;
    });
  }

  public salvarCidade(cidade: any){
    if (!cidade['table']) cidade['table'] = 'cidade';
    this.dbLocal.post(cidade).then((res)=>{
      cidade._rev = res.rev;
    });
  }

  public salvarCliente(cliente: any){
    if (!cliente['table']) cliente['table'] = 'cliente';
    this.dbLocal.post(cliente).then((resCliente)=>{
      cliente._rev = resCliente.rev;
    });
  }


  public salvarRamo(ramo: any){
    if (!ramo['table']) ramo['table'] = 'ramo';
    this.dbLocal.post(ramo).then((resRamo)=>{
      ramo._rev = resRamo.rev;
    });
  }

  public removeCliente(cliente:any){
    this.dbLocal.remove(cliente);
  }
  public removeEstado(estado:any){
    this.dbLocal.remove(estado);
  }
  public removeCidade(cidade:any){
    this.dbLocal.remove(cidade);
  }
  public removeRamo(ramo:any){
    this.dbLocal.remove(ramo);
  }

}
