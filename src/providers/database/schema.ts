export default [
  {
    singular: 'estado',
    plural: 'estados',
    relations: {
      cidades: {
        hasMany: {
          type:'cidade',
          options: {
            async: true
          }
        }
      }
    }
  },
  {
    singular: 'cidade',
    plural: 'cidades',
    relations: {
      ramos: {
        hasMany: {
          type:'ramo',
           options: {
             async: true
           }
        }
      },
      estado:{
        belongsTo:'estado'
      }
    }
  },
  {
    singular: 'ramo',
    plural: 'ramos',
    relations: {
      clientes: {
        hasMany: {type:'cliente', options: {async: true}}
      },
      cidades: {
        hasMany: {type:'cidade', options: {async: true}}
      }
    }
  },
  {
    singular: 'cliente',
    plural: 'clientes',
    relations: {
      ramos: {
        hasMany: {type:'ramo', options: {async: true}}
      },
      cidade: {
        belongsTo: 'cidade'
      },
      estado:{
        belongsTo: 'estado'
      }
    }
  }
]
