import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';

@IonicPage()
@Component({
  selector: 'page-estados-form',
  templateUrl: 'cidadesForm.html',
})
export class CidadesFormPage {
  estados: any;
  ramos: any;
  cidade= {
    estado:"",
    nomeCidade:""
  };

  constructor(
    public params: NavParams,
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public databaseService: DatabaseProvider,
  ) {
  }

  ionViewDidEnter(){
    var cidade = this.params.get('cidade');
    if (cidade) this.cidade = cidade;
    this.databaseService.getEstados().then((dataEstado) =>{
      this.estados = dataEstado['docs'];
    });
    this.databaseService.getRamos().then((dataRamo) =>{
      this.ramos = dataRamo['docs'];
    })

  }

  public dismiss(){
    this.navCtrl.pop();
  }

  public salvarCidade() {
    this.databaseService.salvarCidade(this.cidade);
    this.dismiss();
  };

}
