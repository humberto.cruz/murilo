import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CidadesFormPage } from './cidadesForm';

@NgModule({
  declarations: [
    CidadesFormPage,
  ],
  imports: [
    IonicPageModule.forChild(CidadesFormPage),
  ],
})
export class CidadesFormPageModule {}
