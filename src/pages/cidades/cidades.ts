import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { CidadesFormPage } from './cidadesForm';

@IonicPage()
@Component({
  selector: 'page-cidades',
  templateUrl: 'cidades.html',
})
export class CidadesPage {

  cidades: any;
  estados: any;
  cidade= {
    nomeCidade:'',
    estado:null
  };

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public databaseService: DatabaseProvider,
    public alertCtrl: AlertController,
  ) {
  }

  ionViewDidEnter(){
    this.databaseService.getCidades().then((dataCidade) =>{
      this.cidades = dataCidade['docs'];
    })
    this.databaseService.getEstados().then((dataEstado) =>{
      this.estados = dataEstado['docs'];
    })
  }

  public novaCidade() {
    let modal = this.modalCtrl.create(CidadesFormPage);
    modal.onDidDismiss(data => {
      this.ionViewDidEnter();
    });
    modal.present();
  }

  public editarCidade(cidade){
    let modal = this.modalCtrl.create(CidadesFormPage,{cidade:cidade});
    modal.onDidDismiss(data => {
      this.ionViewDidEnter();
    });
    modal.present();
  }

  public removeCidade(cidade){
    let alert = this.alertCtrl.create({
      title:'Remover Cidade',
      subTitle:'Você tem certeza ?',
      buttons:[
        {
          text:'Não',
          role:'cancel',
          handler: () => {
          }
        },
        {
          text:'Sim',
          handler: ()=>{
            this.databaseService.removeCidade(cidade);
          }
        }
      ]
    });
    alert.present();
  }
}
