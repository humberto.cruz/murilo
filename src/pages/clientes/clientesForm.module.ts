import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientesFormPage } from './clientesForm';

@NgModule({
  declarations: [
    ClientesFormPage,
  ],
  imports: [
    IonicPageModule.forChild(ClientesFormPage),
  ],
})
export class ClientesFormPageModule {}
