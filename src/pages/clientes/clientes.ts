import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { ClientesFormPage } from './clientesForm';

@IonicPage()
@Component({
  selector: 'page-clientes',
  templateUrl: 'clientes.html',
})
export class ClientesPage {

  clientes: any;
  ramos: any;
  cliente= {
    nomeCliente:'',
    ramo:null
  };

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public databaseService: DatabaseProvider,
    public alertCtrl: AlertController,
  ) {
  }

  ionViewDidEnter(){
    this.databaseService.getClientes().then((dataCliente) =>{
      this.clientes = dataCliente['docs'];
    })
    this.databaseService.getRamos().then((dataRamo) =>{
      this.ramos = dataRamo['docs'];
    })
  }

  public novoCliente() {
    let modal = this.modalCtrl.create(ClientesFormPage);
    modal.onDidDismiss(data => {
      this.ionViewDidEnter();
    });
    modal.present();
  }

  public editarCliente(cliente){
    let modal = this.modalCtrl.create(ClientesFormPage,{cliente:cliente});
    modal.onDidDismiss(data => {
      this.ionViewDidEnter();
    });
    modal.present();
  }

  public removeCliente(cliente){
    let alert = this.alertCtrl.create({
      title:'Remover Cliente',
      subTitle:'Você tem certeza ?',
      buttons:[
        {
          text:'Não',
          role:'cancel',
          handler: () => {
          }
        },
        {
          text:'Sim',
          handler: ()=>{
            this.databaseService.removeCliente(cliente);
          }
        }
      ]
    });
    alert.present();

  }
}
