import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';

@IonicPage()
@Component({
  selector: 'page-clientes-form',
  templateUrl: 'clientesForm.html',
})
export class ClientesFormPage {

  ramos:any;
  estados:any;
  cidades:any;
  cliente= {
    ramos:[],
    nome:"",
    tel:"",
    tel2:"",
    endereco:"",
    bairro:"",
    email:"",
    site:"",
    cidade:"",
    estado:"",
    data:""

  };

  constructor(
    public params: NavParams,
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public databaseService: DatabaseProvider,
  ) {
  }

  ionViewDidEnter(){
    var cliente = this.params.get('cliente');
    if (cliente) this.cliente = cliente;
    this.databaseService.getRamos().then((dataRamo) =>{
      this.ramos = dataRamo['docs'];
    })
    this.databaseService.getEstados().then((dataEstado) =>{
      this.estados = dataEstado['docs'];
    })
    this.databaseService.getCidades().then((dataCidade) =>{
      this.cidades = dataCidade['docs'];
    })
  }

  public dismiss(){
    this.navCtrl.pop();
  }

  public salvarCliente() {
    this.databaseService.salvarCliente(this.cliente);
    this.dismiss();
  };

}
