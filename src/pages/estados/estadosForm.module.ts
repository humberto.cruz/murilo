import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EstadosFormPage } from './estadosForm';

@NgModule({
  declarations: [
    EstadosFormPage,
  ],
  imports: [
    IonicPageModule.forChild(EstadosFormPage),
  ],
})
export class EstadosFormPageModule {}
