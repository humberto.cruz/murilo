import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { EstadosFormPage } from './estadosForm';


@IonicPage()
@Component({
  selector: 'page-estados',
  templateUrl: 'estados.html',
})
export class EstadosPage {

  items: any;
  estados: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public databaseService: DatabaseProvider,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
  ) {

  }

  ionViewDidEnter(){
    this.databaseService.getEstados().then((data) =>{
      this.items = data['docs'];
    })
  }

  public novoEstado() {
    let modal = this.modalCtrl.create(EstadosFormPage);
    modal.onDidDismiss(data => {
      this.ionViewDidEnter();
    });
    modal.present();
  }

  public editarEstado(estado){
    let modal = this.modalCtrl.create(EstadosFormPage,{estado:estado});
    modal.onDidDismiss(data => {
      this.ionViewDidEnter();
    });
    modal.present();
  }

  public removeEstado(estado){
    let alert = this.alertCtrl.create({
      title:'Remover Estado',
      subTitle:'Você tem certeza ?',
      buttons:[
        {
          text:'Não',
          role:'cancel',
          handler: () => {
          }
        },
        {
          text:'Sim',
          handler: ()=>{
            this.databaseService.removeEstado(estado);
          }
        }
      ]
    });
    alert.present();

  }

}
