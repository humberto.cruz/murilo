import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';

@IonicPage()
@Component({
  selector: 'page-estados-form',
  templateUrl: 'estadosForm.html',
})
export class EstadosFormPage {

  estados: {estados:['']};
  estado= {
    siglaEstado:"",
    nomeEstado:""
  };

  constructor(
    public params: NavParams,
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public databaseService: DatabaseProvider,
  ) {
  }

  ionViewDidEnter(){
    var estado = this.params.get('estado');
    if (estado) this.estado = estado;
  }

  public dismiss(){
    this.navCtrl.pop();
  }

  public editarEstado(estado){
    this.estado = estado;
  }

  public removeEstado(estado){
    this.databaseService.removeEstado(estado);
  }

  public salvarEstado() {
    this.databaseService.salvarEstado(this.estado);
    this.dismiss();
  };

}
