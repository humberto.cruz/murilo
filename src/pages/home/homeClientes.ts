import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';


@Component({
  selector: 'page-home-clientes',
  templateUrl: 'homeClientes.html'
})
export class HomeClientesPage {
  clientes:any;
  constructor(
    public navCtrl: NavController,
    public params: NavParams,
    public database: DatabaseProvider) {
  }

  ionViewDidEnter(){
      var ramo = this.params.get('ramo');
      var cidade = this.params.get('cidade');
      this.database.getClientes({
        cidade:cidade._id,
        ramos:{'$elemMatch':{'$eq':ramo._id}}
      }).then((dataCliente) =>{
        this.clientes = dataCliente['docs'];
      });
  }
}
