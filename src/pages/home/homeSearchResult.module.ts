import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomeSearchResultPage } from './homeSearchResult';

@NgModule({
  declarations: [
    HomeSearchResultPage,
  ],
  imports: [
    IonicPageModule.forChild(HomeSearchResultPage),
  ],
})
export class HomeSearchResultPageModule {}
