import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { HomeClientesPage } from './homeClientes';

import { DatabaseProvider } from '../../providers/database/database';


@Component({
  selector: 'page-home-ramos',
  templateUrl: 'homeRamos.html'
})
export class HomeRamosPage {
  ramos:any;
  cidade:any;
  constructor(
    public navCtrl: NavController,
    public params: NavParams,
    public database: DatabaseProvider) {
  }

  ionViewDidEnter(){
      this.cidade = this.params.get('cidade');
      this.database.getRamos({'_id':{'$in':this.cidade.ramos}}).then((dataRamo) =>{
        this.ramos = dataRamo['docs'];
      })
  }

  openHomeClientes(ramo){
    this.navCtrl.push(HomeClientesPage,{ramo:ramo,cidade:this.cidade});
  }
}
