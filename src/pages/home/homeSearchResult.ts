import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';


@Component({
  selector: 'page-home-search-result',
  templateUrl: 'homeSearchResult.html'
})
export class HomeSearchResultPage {
  clientes:any;
  constructor(
    public navCtrl: NavController,
    public params: NavParams,
    public database: DatabaseProvider) {
  }

  ionViewDidEnter(){
      var cliente = this.params.get('cliente');
      var reg = cliente.nomeCliente;
      this.database.getClientes({
        nome:{'$regex':new RegExp(reg,'g')},
      }).then((dataCliente) =>{
        console.log(dataCliente);
        this.clientes = dataCliente['docs'];
      });
  }
}
