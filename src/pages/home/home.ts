import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { HomeCidadesPage } from './homeCidades';
import { DatabaseProvider } from '../../providers/database/database';

import { HomeSearchPage } from './homeSearch';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  estados:any;

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public database: DatabaseProvider) {
  }

  public searchClientes() {
    let modal = this.modalCtrl.create(HomeSearchPage);
    modal.present();
  }

  ionViewDidEnter(){
    this.database.getEstados().then((data) =>{
      this.estados = data['docs'];
    }).catch((err)=>console.log(err))
  }


  openHomeCidades(estado){
    this.navCtrl.push(HomeCidadesPage,{estado:estado});
  }

}
