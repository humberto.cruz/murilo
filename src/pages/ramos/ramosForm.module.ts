import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RamosFormPage } from './ramosForm';

@NgModule({
  declarations: [
    RamosFormPage,
  ],
  imports: [
    IonicPageModule.forChild(RamosFormPage),
  ],
})
export class RamosFormPageModule {}
