import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { RamosFormPage } from './ramosForm';


@IonicPage()
@Component({
  selector: 'page-ramos',
  templateUrl: 'ramos.html',
})
export class RamosPage {

  ramos: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public databaseService: DatabaseProvider,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
  ) {
  }

  ionViewDidEnter(){
    this.databaseService.getRamos({}).then((dataRamo) =>{
      this.ramos = dataRamo['docs'];
    })
  }

  public novoRamo() {
    let modal = this.modalCtrl.create(RamosFormPage);
    modal.onDidDismiss(data => {
      this.ionViewDidEnter();
    });
    modal.present();
  }

  public editarRamo(ramo){
    let modal = this.modalCtrl.create(RamosFormPage,{ramo:ramo});
    modal.onDidDismiss(data => {
      this.ionViewDidEnter();
    });
    modal.present();
  }

  public removeRamo(ramo){
    let alert = this.alertCtrl.create({
      title:'Remover Ramo',
      subTitle:'Você tem certeza ?',
      buttons:[
        {
          text:'Não',
          role:'cancel',
          handler: () => {
          }
        },
        {
          text:'Sim',
          handler: ()=>{
            this.databaseService.removeRamo(ramo);
          }
        }
      ]
    });
    alert.present();
  }
}
