import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';

@IonicPage()
@Component({
  selector: 'page-ramos-form',
  templateUrl: 'ramosForm.html',
})
export class RamosFormPage {

  ramos: any;
  cidades:any;
  ramo= {
    nomeRamo:""
  };

  constructor(
    public params: NavParams,
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public databaseService: DatabaseProvider,
  ) {
    var ramo = this.params.get('ramo');
    if (ramo) this.ramo = ramo;
    this.databaseService.getCidades().then((dataCidade) =>{
      this.cidades = dataCidade['docs'];
    })
  }

  public dismiss(){
    this.navCtrl.pop();
  }

  public editarRamo(ramo){
    this.ramo = ramo;
  }

  public removeRamo(ramo){
    this.databaseService.removeRamo(ramo);
  }

  public salvarRamo() {
    console.log(this.ramo);
    this.databaseService.salvarRamo(this.ramo);
    this.dismiss();
  };

}
