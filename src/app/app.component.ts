import { Component } from '@angular/core';

import {App} from 'ionic-angular';

import { Platform } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';

import { EstadosPage } from '../pages/estados/estados';
import { CidadesPage } from '../pages/cidades/cidades';
import { RamosPage } from '../pages/ramos/ramos';
import { ClientesPage } from '../pages/clientes/clientes';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(
    private app: App,
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
  openHome(){
    let navs = this.app.getActiveNavs();
    navs[0].push(HomePage);
  }
  openEstados(){
    let navs = this.app.getActiveNavs();
    navs[0].push(EstadosPage);
  }
  openCidades(){
    let navs = this.app.getActiveNavs();
    navs[0].push(CidadesPage);
  }
  openRamos(){
    let navs = this.app.getActiveNavs();
    navs[0].push(RamosPage);
  }
  openClientes(){
    let navs = this.app.getActiveNavs();
    navs[0].push(ClientesPage);
  }
}
