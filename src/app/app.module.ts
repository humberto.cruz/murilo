import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { HomeCidadesPage } from '../pages/home/homeCidades';
import { HomeRamosPage } from '../pages/home/homeRamos';
import { HomeClientesPage } from '../pages/home/homeClientes';

import { DatabaseProvider } from '../providers/database/database';

import { ClientesPage } from '../pages/clientes/clientes';
import { ClientesFormPage } from '../pages/clientes/clientesForm';
import { RamosPage } from '../pages/ramos/ramos';
import { RamosFormPage } from '../pages/ramos/ramosForm';
import { CidadesPage } from '../pages/cidades/cidades';
import { CidadesFormPage } from '../pages/cidades/cidadesForm';
import { EstadosPage } from '../pages/estados/estados';
import { EstadosFormPage } from '../pages/estados/estadosForm';

import { HomeSearchPage } from '../pages/home/homeSearch';
import { HomeSearchResultPage } from '../pages/home/homeSearchResult';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    HomeCidadesPage,
    HomeRamosPage,
    HomeClientesPage,
    ClientesPage,
    ClientesFormPage,
    RamosPage,
    RamosFormPage,
    CidadesPage,
    CidadesFormPage,
    EstadosPage,
    EstadosFormPage,
    HomeSearchPage,
    HomeSearchResultPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    HomeCidadesPage,
    HomeRamosPage,
    HomeClientesPage,
    ClientesPage,
    ClientesFormPage,
    RamosPage,
    RamosFormPage,
    CidadesPage,
    CidadesFormPage,
    EstadosPage,
    EstadosFormPage,
    HomeSearchPage,
    HomeSearchResultPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DatabaseProvider
  ]
})
export class AppModule {}
